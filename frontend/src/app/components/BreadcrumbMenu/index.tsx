import * as React from 'react';
import '../../style.local.css';
import style from '../../style.local.css';

export declare namespace BreadCrumbMenu {
  export interface Props {
    menu: Array<MenuItem>;
  }

  export interface MenuItem {
    text: string;
    func?: React.MouseEventHandler<Element>;
    style?: object;
  }
}

const renderMenuItems = (menu: Array<BreadCrumbMenu.MenuItem>) =>
  menu.map((menuItem, index) => (
    <li key={index}>
      <a style={menuItem.style} onClick={menuItem.func}>
        {menuItem.text}
      </a>
    </li>
  ));

export const BreadcrumbMenu = (props: BreadCrumbMenu.Props) => {
  return <ul className={style.breadcrumbs}>{renderMenuItems(props.menu)}</ul>;
};
