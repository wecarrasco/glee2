import * as React from 'react';
import { withKnobs } from '@storybook/addon-knobs';
import { ActionsMenu } from './index';
import { EmployeeActions } from '../../containers/Employee/actions';
import { createMemoryHistory } from 'history';
import { match } from 'react-router';

export default {
  title: 'ActionsMenu',
  component: ActionsMenu,
  decorator: [withKnobs],
};

const employeeId = '1';
const history = createMemoryHistory();
const location = history.location;
const matchObj: match = {
  params: {},
  isExact: true,
  path: '/',
  url: '/',
};

export const EmployeeActionsMenu = () => (
  <ActionsMenu
    history={history}
    location={location}
    match={matchObj}
    employeeId={employeeId}
    isActive={true}
    actions={EmployeeActions}
  />
);
