Feature: remove employee
    In order to \_____ as a \______ I can \______.

  @WIP
  Scenario: Activate Employee by Id (uid:5bf115db-50bd-496a-bf64-fb2215ccde56)
    Given an inactive employee
    When I activate an employee by the Id
    Then I should be able to see the same employee active

  @WIP
  Scenario: Deactivate Employee by Id (uid:384c2cf5-f5d9-4146-8faa-5709827cd355)
    Given an active employee
    When I deactivate an employee by the Id
    Then I not should be able to see the same employee active
