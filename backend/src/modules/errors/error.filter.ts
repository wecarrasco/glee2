import {
  ExceptionFilter,
  Catch,
  HttpException,
  ArgumentsHost,
  HttpStatus,
} from '@nestjs/common';
import { Response } from 'express';

@Catch()
export class ErrorFilter implements ExceptionFilter {
  catch(error: Error, host: ArgumentsHost): Response<any> {
    const response = host.switchToHttp().getResponse<Response>();

    if (error instanceof HttpException) {
      const message = error.getResponse();
      return response
        .status(error.getStatus())
        .send(typeof message === 'string' ? { message } : message);
    }

    if (error.name === 'EntityNotFound') {
      return response.status(HttpStatus.NOT_FOUND).send({
        statusCode: HttpStatus.NOT_FOUND,
        message: error.message,
      });
    }

    return response.status(HttpStatus.INTERNAL_SERVER_ERROR).send({
      statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
      message: error.message,
      stack: error.stack,
    });
  }
}
