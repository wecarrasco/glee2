import { IEventHandler as EventHandler } from '@nestjs/cqrs';
import { DomainEvent } from './domainEvent';

export type IEventHandler<TEvent extends DomainEvent> = EventHandler<TEvent>
