import {
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  CreateDateColumn,
} from 'typeorm';
import { AggregateRoot } from './aggregateRoot';

export const getCurrentTimestamp = () => 'CURRENT_TIMESTAMP';

export abstract class BaseEntity extends AggregateRoot<number> {
  @PrimaryGeneratedColumn()
  id: number;

  @CreateDateColumn({ type: 'timestamptz', default: getCurrentTimestamp })
  createdAt: Date;

  @UpdateDateColumn({ type: 'timestamptz', default: getCurrentTimestamp })
  updatedAt: Date;
}
