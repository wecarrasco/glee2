output "ec2_id" {
  description = "The id of the vpc"
  value       = aws_instance.ec2-first.id
}
