import psycopg2
import os
from psycopg2 import sql
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

con = psycopg2.connect(
        dbname='postgres',
        user=os.environ['RDS_DB_USER'],
        host=os.environ['RDS_DB_HOST'],
        password=os.environ['RDS_DB_PASSWORD']
    )

con.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

cur = con.cursor()

if_exists = "SELECT 1 FROM pg_catalog.pg_database WHERE datname = '{}'".format(os.environ['NAME_PREFIX'])
cur.execute(if_exists)
exists = cur.fetchone()
if not exists:
    cur.execute(sql.SQL("CREATE DATABASE \"{}\"".format(os.environ['NAME_PREFIX'])))
    con2 = psycopg2.connect(
        dbname=os.environ['NAME_PREFIX'],
        user=os.environ['RDS_DB_USER'],
        host=os.environ['RDS_DB_HOST'],
        password=os.environ['RDS_DB_PASSWORD']
    )
    con2.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    cur2 = con2.cursor()
    cur2.execute(sql.SQL("CREATE EXTENSION IF NOT EXISTS \"uuid-ossp\""))