﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;

namespace Web.TestingSupport.Support
{
    [SuppressMessage("ReSharper", "EF1000")]
    [Route("crud/[Controller]")]
    [ApiController]
    public abstract class CrudController<T> where T : class
    {
        readonly AppDataContext _context;
        readonly string _table;

        protected CrudController(AppDataContext context)
        {
            _context = context;
            _table = GetType().Name.Replace("Controller", "");
        }

        [HttpPost]
        public async Task Add(Dictionary<string, string> properties)
        {
            await Insert(properties);
        }

        async Task Insert(Dictionary<string, string> properties)
        {
            try
            {
                var keyNames = string.Join(",", properties.Select(x => x.Key));
                var valueParamNames = string.Join(',', properties.Select(x => $"@{x.Key}"));
                var commandText =
                    $"INSERT INTO {_table} ({keyNames}) VALUES ({valueParamNames})";
                var sqlParameters = properties.Select(x => GetSqlParameter(x.Key, x.Value)).ToList();
                await ExecuteCommand(commandText, sqlParameters);
            }
            catch (Exception ex)
            {
                throw new Exception("Error when inserting.", ex);
            }
        }

        SqliteParameter GetSqlParameter(string key, string value)
        {
            if (value == null) throw new ArgumentNullException(nameof(key));

            var sqliteType = GetSqlType(key);
            var sqlValue = GetSqlValue(key, value);
            return new SqliteParameter($"@{key}", sqliteType) {Value = sqlValue};
        }

        object GetSqlValue(string key, string value)
        {
            var propType = GetPropertyType(key);
            if (propType == typeof(Guid)) return new Guid(value).ToByteArray();

            return value;
        }

        static SqliteType GetSqlType(string key)
        {
            var type = GetPropertyType(key);
            var sqlType = SqliteType.Blob;
            if (type == typeof(string)) sqlType = SqliteType.Text;
            else if (type == typeof(Guid)) sqlType = SqliteType.Blob;
            else if (type == typeof(int)) sqlType = SqliteType.Integer;
            else if (type == typeof(double)) sqlType = SqliteType.Real;
            else if (type == typeof(bool)) sqlType = SqliteType.Integer;
            return sqlType;
        }

        static Type GetPropertyType(string key)
        {
            try
            {
                var t = typeof(T);
                var prop = t.GetProperties()
                    .First(x => string.Equals(x.Name, key, StringComparison.CurrentCultureIgnoreCase));
                return prop.PropertyType;
            }
            catch (Exception)
            {
                throw new Exception($"Property {key} was not found on type {typeof(T).Name}");
            }
        }

        [HttpDelete]
        public async Task ClearAll()
        {
            await Delete();
        }

        async Task Delete()
        {
            var commandText = $"DELETE FROM {_table}";
            await ExecuteCommand(commandText, new List<SqliteParameter>());
        }

        [HttpDelete]
        [Route("{id}")]
        public async Task ClearOne([FromRoute] Guid? id)
        {
            await Delete(id);
        }

        async Task Delete(Guid? id)
        {
            if (!id.HasValue) throw new ArgumentNullException(nameof(id));

            var commandText = $"DELETE FROM {_table} WHERE Id=@id";
            var parameter = new SqliteParameter("@id", id.Value.ToString());
            await ExecuteCommand(commandText, new List<SqliteParameter> {parameter});
        }

        [HttpGet]
        public async Task<List<T>> GetAll([FromQuery] Dictionary<string, string> parameters)
        {
            return await SelectWhere(parameters);
        }

        async Task<List<T>> SelectWhere(Dictionary<string, string> parameters)
        {
            var conditions = parameters.Select(x => $"{x.Key}=@{x.Key}").ToList();
            var where = conditions.Any() ? string.Join(" AND ", conditions) : "1=1";
            var commandText = $"SELECT * FROM {_table} WHERE {@where}";
            var sqlParameters = parameters.Select(x => GetSqlParameter(x.Key, x.Value)).ToList();
            var all = await ExecuteQuery(commandText, sqlParameters);
            return all.ToList();
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<T> GetOne([FromRoute] Guid? id)
        {
            return await Select(id);
        }

        async Task<T> Select(Guid? id)
        {
            if (!id.HasValue) throw new ArgumentNullException(nameof(id));

            var item = await GetItemWithSqlParameter(id.Value) ?? await GetItemWithRawSql(id.Value);
            if (item == null) throw new Exception($"No item was not found for id '{id.Value}'.");
            return item;
        }

        async Task<T> GetItemWithRawSql(Guid id)
        {
            var raw16Id = BitConverter.ToString(id.ToByteArray()).Replace("-", "").ToLower();
            var commandText = $"SELECT * FROM {_table} WHERE Id=X'{raw16Id}'";
            var items = await ExecuteQuery(commandText, new List<SqliteParameter>());
            var item = items.FirstOrDefault();
            return item;
        }

        async Task<T> GetItemWithSqlParameter(Guid id)
        {
            var commandText = $"SELECT * FROM {_table} WHERE Id=@id";
            var sqliteParameter = new SqliteParameter("@id", SqliteType.Text)
                {Value = id};
            var items = await ExecuteQuery(commandText, new List<SqliteParameter> {sqliteParameter});
            var item = items.FirstOrDefault();
            return item;
        }

        [HttpPut]
        [Route("{id}")]
        public async Task Modify([FromRoute] Guid? id, [FromBody] Dictionary<string, string> properties)
        {
            await Update(id, properties);
        }

        async Task Update(Guid? id, Dictionary<string, string> properties)
        {
            if (!id.HasValue) throw new ArgumentNullException(nameof(id));

            var nonNullChanges = properties
                .Where(x => x.Value != null)
                .Where(x => x.Key != "id")
                .ToList();

            var changes = nonNullChanges.Select(x => $"{x.Key}=@{x.Key}").ToList();
            var sqlParameters = nonNullChanges.Select(x => GetSqlParameter(x.Key, x.Value)).ToList();
            sqlParameters.Add(GetSqlParameter("id", id.ToString()));
            var commandText = $"UPDATE {_table} SET {string.Join(",", changes.ToArray())} WHERE id=@id";
            await ExecuteCommand(commandText, sqlParameters, 1);
        }

        async Task<IEnumerable<T>> ExecuteQuery(string commandText, IEnumerable<SqliteParameter> parameters)
        {
            try
            {
                Console.WriteLine("CRUD:" + commandText);
                var @out = await _context.Set<T>().FromSql(commandText, parameters.ToArray()).ToListAsync();
                return @out;
            }
            catch (Exception e)
            {
                throw new Exception($"{e.Message} SQL query: {commandText}", e);
            }
        }

        async Task ExecuteCommand(string commandText, IEnumerable<SqliteParameter> parameters,
            int expectedRowsAffected = 0)
        {
            try
            {
                Console.WriteLine("CRUD:" + commandText);
                var rowsAffected = await _context.Database.ExecuteSqlCommandAsync(commandText, parameters);
                if (rowsAffected < expectedRowsAffected)
                    throw new Exception(
                        $"SQL command was expected to affect {expectedRowsAffected} but affected {rowsAffected}.");
            }
            catch (Exception e)
            {
                throw new Exception($"{e.Message} SQL command: {commandText}", e);
            }
        }
    }
}