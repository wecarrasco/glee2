namespace Domain.Common.Services
{
    public interface IIdentityGenerator<out T>
    {
        T Generate();
    }
}