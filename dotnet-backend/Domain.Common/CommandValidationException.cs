using System;
using System.Collections.Generic;
using System.Linq;
using FluentValidation.Results;

namespace Domain.Common
{
    public class CommandValidationException : Exception
    {
        public IList<ValidationFailure> ValidationFailures { get; }

        public CommandValidationException(IList<ValidationFailure> validationFailures, string message = "") : base(
            string.IsNullOrEmpty(message)
                ? $"Validation failed for the following reasons: {Reasons(validationFailures)}"
                : message)
        {
            ValidationFailures = validationFailures;
        }

        protected static string Reasons(IList<ValidationFailure> validationFailures)
        {
            return string.Join(", ", validationFailures.Select(x => $"{x.PropertyName}: {x.ErrorMessage}"));
        }
    }

    public class CommandValidationException<TCommand> : CommandValidationException
    {
        public CommandValidationException(IList<ValidationFailure> validationFailures) : base(validationFailures,
            $"Validation of {typeof(TCommand)} failed for the following reasons: {Reasons(validationFailures)}")
        {
        }
    }
}