using System;
using System.Linq.Expressions;
using Domain.Common;
using FluentAssertions;
using FluentValidation.Results;

namespace Testing.Utilities
{
    public static class ValidationExceptionAssertionExtensions
    {
        public static void ShouldIncludeValidationFailureLike(this Exception ex,
            Expression<Func<ValidationFailure, bool>> expression)
        {
            if (ex == null) 
                throw new Exception("There was no exception.");
            if (!(ex is CommandValidationException))
                throw new Exception($"The exception was a {ex.GetType().Name}, not a CommandValidationException", ex);

            var commandValidationException = ex.As<CommandValidationException>();
            commandValidationException.ValidationFailures.Should().Contain(expression);
        }
    }
}