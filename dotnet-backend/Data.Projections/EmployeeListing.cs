using System;
using Avenue.Domain;

namespace Data.Projections
{
    public class EmployeeListing : IProjection
    {
        protected EmployeeListing()
        {
        }

        public EmployeeListing(Guid id, string firstName, string? middleName, string lastName, string? secondLastName,
            string? displayName, string companyEmail, string? personalEmail, DateTime birthDate, DateTime? startDate,
            string address, string phoneNumber, string bankName, string accountNumber, string gender, string tags,
            string country, string region, string city, double? salary, string? salaryType, DateTime? effectiveDate, 
            bool isActive)
        {
            Id = id;
            FirstName = firstName;
            MiddleName = middleName;
            LastName = lastName;
            SecondLastName = secondLastName;
            DisplayName = displayName;
            CompanyEmail = companyEmail;
            PersonalEmail = personalEmail;
            Birthdate = birthDate;
            StartDate = startDate;
            Address = address;
            PhoneNumber = phoneNumber;
            BankName = bankName;
            AccountNumber = accountNumber;
            Gender = gender;
            Tags = tags;
            Country = country;
            Region = region;
            City = city;
            Salary = salary;
            SalaryType = salaryType;
            EffectiveDate = effectiveDate;
            IsActive = isActive;
        }

        public string Gender { get; protected set; } = "";
        public Guid Id { get; protected set; } = Guid.Empty;
        public string FirstName { get; protected set; } = "";
        public string? MiddleName { get; protected set; }
        public string LastName { get; protected set; } = "";
        public string? SecondLastName { get; protected set; }
        public string? DisplayName { get; protected set; }
        public string CompanyEmail { get; protected set; } = "";
        public string? PersonalEmail { get; protected set; }
        public DateTime Birthdate { get; protected set; } = DateTime.MinValue;
        public DateTime? StartDate { get; protected set; }
        public string Address { get; protected set; } = "";
        public string PhoneNumber { get; protected set; } = "";
        public string BankName { get; protected set; } = "";
        public string AccountNumber { get; protected set; } = "";
        public string Tags { get; protected set; } = "";
        public string Country { get; protected set; } = "";
        public string Region { get; protected set; } = "";
        public string City { get; protected set; } = "";
        public double? Salary { get; protected set; }
        public DateTime? EffectiveDate { get; protected set; }
        public string? SalaryType { get; protected set; }
        public bool Removed { get; protected set; } = false;

        public bool IsActive { get; set; } = true;

        public void Remove()
        {
            Removed = true;
        }

        
    }
}