namespace Web.Employees.Requests
{
    public class UpdateNamesRequest
    {
        public string FirstName { get; set; } = "";
        public string LastName { get; set; } = "";
        public string MiddleName { get; set; } = "";
        public string SecondLastName { get; set; } = "";
    }
}