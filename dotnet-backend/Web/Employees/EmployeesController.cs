using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Avenue.Commands;
using Avenue.Dispatch;
using Avenue.Domain;
using Data.Projections;
using Domain.Common.Services;
using Domain.Employees;
using Domain.Employees.Commands;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Protocols;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.IdentityModel.Tokens;
using Web.Employees.Requests;
using Web.Employees.Responses;

namespace Web.Employees
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class EmployeesController : ControllerBase
    {

        readonly IDispatcher<ICommand> _commandDispatcher;
        readonly IIdentityGenerator<Guid> _identityGenerator;
        //EE12: START
        readonly IReadOnlyRepository<EmployeeListing> _readOnlyRepository;
        //EE12: END
        //RP12: START
        // readonly IReadOnlyRepository<Employee> _readOnlyRepository;
        //RP12: END

        public EmployeesController(IDispatcher<ICommand> commandDispatcher,
            //EE13: START
            IReadOnlyRepository<EmployeeListing> readOnlyRepository,
            //EE13: END
            //RP13: START
            // IReadOnlyRepository<Employee> readOnlyRepository,
            //RP13: END
            IIdentityGenerator<Guid> identityGenerator)
        {
            _commandDispatcher = commandDispatcher;
            _readOnlyRepository = readOnlyRepository;
            _identityGenerator = identityGenerator;
        }

        [HttpPost]
        public async Task<NewEmployeeResponse> CreateEmployee(
            [FromBody] CreateEmployeeRequest createCreateEmployeeRequest)
        {
            if (createCreateEmployeeRequest == null)
                throw new ArgumentNullException(nameof(createCreateEmployeeRequest));
            var id = _identityGenerator.Generate();

            await _commandDispatcher.Dispatch(new CreateEmployee(id, createCreateEmployeeRequest.FirstName,
                createCreateEmployeeRequest.DisplayName, createCreateEmployeeRequest.LastName,
                createCreateEmployeeRequest.Gender,
                createCreateEmployeeRequest.Salary, createCreateEmployeeRequest.MiddleName,
                createCreateEmployeeRequest.SecondLastName,
                createCreateEmployeeRequest.CompanyEmail, createCreateEmployeeRequest.PersonalEmail,
                createCreateEmployeeRequest.Birthdate,
                createCreateEmployeeRequest.StartDate, createCreateEmployeeRequest.Address,
                createCreateEmployeeRequest.PhoneNumber,
                createCreateEmployeeRequest.BankName, createCreateEmployeeRequest.AccountNumber,
                createCreateEmployeeRequest.Tags,
                createCreateEmployeeRequest.Country, createCreateEmployeeRequest.Region,
                createCreateEmployeeRequest.City,
                createCreateEmployeeRequest.EffectiveDate, createCreateEmployeeRequest.SalaryType));

            return new NewEmployeeResponse(id);
        }

        int GetValueOrDefault(IDictionary<string, string> parameters, string key, int @default)
        {
            var item = parameters.Where(x => string.Equals(x.Key, key, StringComparison.CurrentCultureIgnoreCase))
                .ToList();
            return !item.Any() ? @default : Convert.ToInt32(item.First().Value);
        }

        [HttpGet]
        [Route("{id}")]
        //EE14:START
        public async Task<EmployeeListing> GetOne([FromRoute] Guid id)
        //EE14:END
        //RP14:START
        // public async Task<Employee> GetOne([FromRoute] Guid id)
        //RP14:END
        {
            return await _readOnlyRepository.GetById(id);
        }

        [HttpGet]
        //EE15:START
        public async Task<EmployeesResponse> GetAll([FromQuery] IDictionary<string, string> parameters)
        //EE15:END
        //RP15:START
        // public async Task<IEnumerable<Employee>> GetAll([FromQuery] IDictionary<string, string> parameters)
        //RP15:END
        {
            var response = new EmployeesResponse();
            var pageSize = GetValueOrDefault(parameters, "pageSize", 10);
            var pageNumber = GetValueOrDefault(parameters, "pageNumber", 1);
            
            var employees = _readOnlyRepository.Set();

            var filterParam = parameters.FirstOrDefault(x =>
                !string.Equals(x.Key, "pageSize", StringComparison.CurrentCultureIgnoreCase) &&
                !string.Equals(x.Key, "pageNumber", StringComparison.CurrentCultureIgnoreCase) ).Value;
            
            if (filterParam != null)
            {
                employees = employees.Where(x =>
                    x.DisplayName!.Contains(filterParam,StringComparison.CurrentCultureIgnoreCase));
            }

            response.Total = employees.Count();
            response.CurrentPage = pageNumber;
            response.PerPage = pageSize;
            response.LastPage = pageSize==0 ? 1 : (int) Math.Ceiling(response.Total /(double)response.PerPage);
            var paged = await employees.Skip(pageSize * (pageNumber - 1))
                .Take(pageSize).ToListAsync();
            response.Data = paged;
             
            return response;
        }

        [HttpPut]
        [Route("{id}/names")]
        public async Task ChangeNames([FromRoute] Guid id, [FromBody] UpdateNamesRequest request)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));
            var command = new ChangeEmployeeNames(id,
                request.FirstName, request.MiddleName, request.LastName, request.SecondLastName);

            await _commandDispatcher.Dispatch(command);
        }

        [HttpPut]
        [Route("{id}/address")]
        public async Task ChangeAddress([FromRoute] Guid id, [FromBody] UpdateAddressRequest updateEmployeeRequest)
        {
            if (updateEmployeeRequest == null)
                throw new ArgumentNullException(nameof(updateEmployeeRequest));
            await _commandDispatcher.Dispatch(new ChangeEmployeeAddress(id,
                updateEmployeeRequest.Address, updateEmployeeRequest.City, updateEmployeeRequest.Region,
                updateEmployeeRequest.Country));
        }

        [HttpPut]
        [Route("{id}/displayName")]
        public async Task ChangeDisplayName([FromRoute] Guid id, [FromBody] UpdateDisplayNameRequest updateDisplayNameRequest)
        {
            if (updateDisplayNameRequest == null)
                throw new ArgumentNullException(nameof(updateDisplayNameRequest));
            await _commandDispatcher.Dispatch(new ChangeEmployeeDisplayName(id,
                updateDisplayNameRequest.DisplayName));
        }

        [HttpPut]
        [Route("{id}/tags")]
        public async Task ChangeTags([FromRoute] Guid id, [FromBody] UpdateEmployeeRequest updateEmployeeRequest)
        {
            if (updateEmployeeRequest == null)
                throw new ArgumentNullException(nameof(updateEmployeeRequest));
            await _commandDispatcher.Dispatch(new ChangeEmployeeTags(id,
                updateEmployeeRequest.Tags));
        }

        [HttpPut]
        [Route("{id}/phoneNumber")]
        public async Task ChangePhone([FromRoute] Guid id, [FromBody] UpdateEmployeeRequest updateEmployeeRequest)
        {
            if (updateEmployeeRequest == null)
                throw new ArgumentNullException(nameof(updateEmployeeRequest));
            await _commandDispatcher.Dispatch(new ChangeEmployeePhoneNumber(id,
                updateEmployeeRequest.PhoneNumber));
        }

        [HttpPut]
        [Route("{id}/personalEmail")]
        public async Task ChangePersonalEmail([FromRoute] Guid id,
            [FromBody] UpdateEmployeeRequest updateEmployeeRequest)
        {
            if (updateEmployeeRequest == null)
                throw new ArgumentNullException(nameof(updateEmployeeRequest));
            await _commandDispatcher.Dispatch(new ChangeEmployeePersonalEmail(id,
                updateEmployeeRequest.PersonalEmail));
        }

        [HttpPut]
        [Route("{id}/companyEmail")]
        public async Task ChangeCompanyEmail([FromRoute] Guid id,
            [FromBody] UpdateEmployeeRequest updateEmployeeRequest)
        {
            if (updateEmployeeRequest == null)
                throw new ArgumentNullException(nameof(updateEmployeeRequest));
            await _commandDispatcher.Dispatch(new ChangeEmployeeCompanyEmail(id,
                updateEmployeeRequest.CompanyEmail));
        }

        [HttpPut]
        [Route("{id}/salary")]
        public async Task ChangeSalary([FromRoute] Guid id,
            [FromBody] UpdateEmployeeRequest updateEmployeeRequest)
        {
            if (updateEmployeeRequest == null)
                throw new ArgumentNullException(nameof(updateEmployeeRequest));
            await _commandDispatcher.Dispatch(new ChangeEmployeeSalary(id,
                updateEmployeeRequest.Salary));
        }

        [HttpPut]
        [Route("{id}/salaryType")]
        public async Task ChangeSalaryType([FromRoute] Guid id,
            [FromBody] UpdateEmployeeRequest updateEmployeeRequest)
        {
            if (updateEmployeeRequest == null)
                throw new ArgumentNullException(nameof(updateEmployeeRequest));
            await _commandDispatcher.Dispatch(new ChangeEmployeeSalaryType(id,
                updateEmployeeRequest.SalaryType));
        }

        [HttpPut]
        [Route("{id}/effectiveDate")]
        public async Task ChangeEffectiveDate([FromRoute] Guid id,
            [FromBody] UpdateEmployeeRequest updateEmployeeRequest)
        {
            if (updateEmployeeRequest == null)
                throw new ArgumentNullException(nameof(updateEmployeeRequest));
            await _commandDispatcher.Dispatch(new ChangeEmployeeEffectiveDate(id,
                updateEmployeeRequest.EffectiveDate));
        }

        [HttpPut]
        [Route("{id}/birthdate")]
        public async Task ChangeBirthdate([FromRoute] Guid id,
            [FromBody] UpdateEmployeeRequest updateEmployeeRequest)
        {
            if (updateEmployeeRequest == null)
                throw new ArgumentNullException(nameof(updateEmployeeRequest));
            await _commandDispatcher.Dispatch(new ChangeEmployeeBirthDate(id,
                updateEmployeeRequest.Birthdate));
        }

        [HttpDelete("{employeeId}")]
        public async Task DeleteEmployee(Guid employeeId)
        {
            await _commandDispatcher.Dispatch(new RemoveEmployee(employeeId));
        }

        [HttpPut("{employeeId}/inactive")]
        public async Task InactivateEmployee(Guid employeeId)
        {
            await _commandDispatcher.Dispatch(new ChangeEmployeeIsActive(employeeId, false));
        }

        [HttpPut("{employeeId}/active")]
        public async Task ActivateEmployee(Guid employeeId)
        {
            await _commandDispatcher.Dispatch(new ChangeEmployeeIsActive(employeeId, true));
        }
    }
}