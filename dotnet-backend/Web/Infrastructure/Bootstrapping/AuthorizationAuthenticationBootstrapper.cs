using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;

namespace Web.Infrastructure.Bootstrapping
{
    public static class AuthorizationAuthenticationBootstrapper
    {
        public static IServiceCollection ConfigureAuthentication(this IServiceCollection services, string issuer, string audience)
        {
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.Authority = issuer;
                options.Audience = audience;
            });

            return services;
        }
    }
}