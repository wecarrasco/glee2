using System;
using Microsoft.Extensions.DependencyInjection;

namespace Web.Infrastructure.Bootstrapping.Dependencies
{
    public static class HandlerDependencyBootstrapper
    {
        public static Action<IServiceCollection> AddCommandHandlerDependencies()
        {
            return services =>
            {
                //add any command handler dependencies here
            };
        }

        public static Action<IServiceCollection> AddEventHandlerDependencies()
        {
            return services =>
            {
                //add any event handler dependencies here
            };
        }
    }
}