using System;
using System.Net;
using System.Threading.Tasks;
using Domain.Common;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace Web.Infrastructure.ExceptionHandling.Renderers
{
    [Boilerplate]
    public class ValidationRender : IExceptionRender
    {
        public bool ShouldHandle(Exception exception)
        {
            return exception is CommandValidationException;
        }

        public Task Render(HttpContext context, Exception exception)
        {
            if(context == null)
                throw new ArgumentNullException(nameof(context));
            if(exception == null)
                throw new ArgumentNullException(nameof(exception));
            
            var notValid = (CommandValidationException) exception;
            var code = HttpStatusCode.BadRequest;
            var result = JsonConvert.SerializeObject(new {errors = notValid.ValidationFailures});
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int) code;
            return context.Response.WriteAsync(result);
        }
    }
}