﻿using System;
using Common.Entities;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;

namespace Data
{
    public class AppDataContext : DbContext, IAppDataContext
    {
        public AppDataContext(DbContextOptions<AppDataContext> options) : base(options)
        {
        }


        public DbSet<Employee> Employees { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var scanner = new TypeScanner.TypeScanner();
            var entitiesTYpe = scanner.GetTypesOf<IEntity>();
            foreach (var entity in entitiesTYpe)
            {
                modelBuilder.Entity(entity);
            }
            base.OnModelCreating(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning));
            base.OnConfiguring(optionsBuilder);
        }
    }
}