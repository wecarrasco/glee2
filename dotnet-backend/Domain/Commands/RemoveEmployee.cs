using System;

namespace Domain.Commands
{
    public class RemoveEmployee
    {
        protected RemoveEmployee()
        {
        }

        public Guid Id { get; private set; }

        public RemoveEmployee(Guid id)
        {
            Id = id;
        }
    }
}