using System;

namespace Domain.Commands
{
    public class ChangeEmployeeAddress
    {
        public ChangeEmployeeAddress(Guid id, string address, string city, string region, string country)
        {
            Id = id;
            Address = address;
            Country = country;
            Region = region;
            City = city;
        }

        public Guid Id { get; }
        public string Address { get; }
        public string Country { get; }
        public string Region { get; }
        public string City { get; }
    }
}