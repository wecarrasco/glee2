using System.Collections.Generic;
using AcklenAvenue.Commands;
using Common.Commands.Validators;

namespace Domain.Commands.Validators.ForCreateEmployee
{
    public class CreateEmployeeValidator : CompositeCommandValidator<CreateEmployee>, ICommandValidator
    {
        public CreateEmployeeValidator(IEnumerable<IValidationComponent<CreateEmployee>> validationComponents) :
            base(validationComponents)
        {
        }
    }
}