using System;
using System.Threading.Tasks;
using AcklenAvenue.Commands;
using Common.Entities;
using Domain.Entities;
using Domain.Repositories;
using Domain.Services;

namespace Domain.Commands.Handlers
{
    public class EmployeeCreator : ICommandHandler<CreateEmployee>
    {
        private readonly IWritableRepository<Employee, Guid> _writableRepository;
        readonly IIdentityGenerator<Guid> _identityGenerator;

        public EmployeeCreator(IWritableRepository<Employee, Guid> writableRepository, IIdentityGenerator<Guid> identityGenerator)
        {
            _writableRepository = writableRepository;
            _identityGenerator = identityGenerator;
        }

        public async Task Handle(CreateEmployee command)
        {
            var employee = new Employee(_identityGenerator.Generate(),command.FirstName, command.MiddleName, command.LastName, command.SecondLastName,
                command.DisplayName, command.Email, command.PersonalEmail, command.Birthdate, command.StartDate,
                command.Address, command.PhoneNumber, command.BankName, command.AccountNumber, command.Gender,
                command.Tags, command.Country, command.Region, command.City, command.Salary, command.EffectiveDate,
                command.SalaryType);

             await _writableRepository.Create(employee);
        }
    }
}