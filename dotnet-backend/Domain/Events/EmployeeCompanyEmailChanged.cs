using System;
using Common.Entities;

namespace Domain.Events
{
    public class EmployeeCompanyEmailChanged : IEvent
    {
        public Guid EmployeeId { get; }
        public string NewCompanyEmail { get; set; }
        public EmployeeCompanyEmailChanged(Guid employeeId, string newCompanyEmail)
        {
            EmployeeId = employeeId;
            NewCompanyEmail = newCompanyEmail;
        }

    }
}