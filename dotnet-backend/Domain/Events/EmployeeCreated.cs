using System;
using Common.Entities;

namespace Domain.Events
{
    public class EmployeeCreated : IEvent
    {
        public Guid EmployeeId { get; protected set; }
        public string FirstName { get; }
        public string MiddleName { get; }
        public string LastName { get; }
        public string SecondLastName { get; }
        public string DisplayName { get; }
        public string Email { get; }
        public string PersonalEmail { get; }
        public DateTime Birthdate { get; }
        public DateTime StartDate { get; }
        public string Address { get; }
        public string PhoneNumber { get; }
        public string BankName { get; }
        public string AccountNumber { get; }
        public string Gender { get; }
        public string Tags { get; }
        public string Country { get; }
        public string Region { get; }
        public string City { get; }
        public double Salary { get; }
        public DateTime EffectiveDate { get; }
        public string SalaryType { get; }
        public string Name { get; protected set; }

      
        public EmployeeCreated(Guid employeeId, string firstName, string middleName, string lastName,
            string secondLastName, string displayName, string email, string personalEmail, DateTime birthdate,
            DateTime startDate, string address, string phoneNumber, string bankName, string accountNumber,
            string gender, string tags, string country, string region, string city, double salary,
            DateTime effectiveDate, string salaryType)
        {
            EmployeeId = employeeId;
            FirstName = firstName;
            MiddleName = middleName;
            LastName = lastName;
            SecondLastName = secondLastName;
            DisplayName = displayName;
            Email = email;
            PersonalEmail = personalEmail;
            Birthdate = birthdate;
            StartDate = startDate;
            Address = address;
            PhoneNumber = phoneNumber;
            BankName = bankName;
            AccountNumber = accountNumber;
            Gender = gender;
            Tags = tags;
            Country = country;
            Region = region;
            City = city;
            Salary = salary;
            EffectiveDate = effectiveDate;
            SalaryType = salaryType;
        }
    }
}