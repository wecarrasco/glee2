using System;
using System.Threading.Tasks;
using AcklenAvenue.Events;

namespace Domain.Events.Handlers
{
    public class NotifyEmployeeAddressChangedToConsole : IEventHandler<EmployeeAddressChanged>
    {
        public Task Handle(EmployeeAddressChanged command)
        {
            Console.WriteLine(
                $"Employee address was changed to {command.Address}, {command.City}, {command.Region}, {command.Country}");
            return Task.CompletedTask;
        }
    }
}