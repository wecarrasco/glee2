using FizzWare.NBuilder;
using Machine.Specifications;
using Moq;
using Web.Infrastructure;
using Web.Status;
using It = Machine.Specifications.It;

namespace Web.Specs.Status
{
    public class when_getting_the_status_of_the_api
    {
        static StatusController _systemUnderTest;
        static ApiInformation _result;
        static IApiInformationGetter _infoGetter;
        static ApiInformation _apiStatusResponse;

        Establish _context = () =>
        {
            _infoGetter = Mock.Of<IApiInformationGetter>();
            _systemUnderTest = new StatusController(_infoGetter);

            _apiStatusResponse = Builder<ApiInformation>.CreateNew().Build();
            Mock.Get(_infoGetter).Setup(x => x.GetInfo()).Returns(_apiStatusResponse);
        };

        Because of = () => { _result = _systemUnderTest.GetStatus(); };

        It should_return_ok = () => { _result.ShouldEqual(_apiStatusResponse); };
    }
}