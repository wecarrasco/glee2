using System;
using System.Collections.Generic;
using System.Linq;
using Data.Projections;
using Domain.Employees;
using FizzWare.NBuilder;
using FluentAssertions;
using Machine.Specifications;
using Moq;
using Testing.Utilities;
using Web.Employees;
using Web.Employees.Responses;
using It = Machine.Specifications.It;

namespace Web.Specs.Employees
{
    [Subject(typeof(EmployeesController))]
    public class when_getting_all_employees_by_parameter : given_an_employee_controller_context
    {
        static Dictionary<string, string> _parameters;

        private Establish _context = () =>
        {
            //EE20:START
            var employees = Builder<EmployeeListing>.CreateListOfSize(2).Build();
            //EE20:END
            //RP20:START
            //var employees = Builder<Employee>.CreateListOfSize(2).Build();
            //RP20:END

            _parameters = new Dictionary<string, string> {{"DisplayName", "DisplayName1"}};

            var queryable = employees.AsAsyncQueryable();
            Mock.Get(_readOnlyRepository).Setup(x => x.Set()).Returns(queryable);

            _expectedResponse = employees.Where(x =>
                x.DisplayName!.Contains(_parameters["DisplayName"], StringComparison.CurrentCultureIgnoreCase)).ToList();
        };

        Because of = async () => { _result = await _employeesController.GetAll(_parameters); };

        It should_return_all_existing_employee = () => { _result.Data.Should().Equal(_expectedResponse); };
        //EE21:START
        static IList<EmployeeListing> _expectedResponse;
        static EmployeesResponse _result;
        //EE21:END
        //RP21:START
        //static IList<Employee> _expectedResponse;
        //static IEnumerable<Employee> _result;
        //RP21:END
    }
}