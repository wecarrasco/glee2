using System;
using Domain.Employees.Commands;
using Machine.Specifications;
using Moq;
using Testing.Utilities;
using Web.Employees;
using It = Machine.Specifications.It;
using ItVerify = Moq.It;

namespace Web.Specs.Employees
{
    [Subject(typeof(EmployeesController))]
    
    public class when_changing_employee_inactivate : given_an_employee_controller_context
    {
        static ChangeEmployeeIsActive _expectedCommand;
        static Guid _employeeId;

        Establish context = () =>
        {
            _employeeId = Guid.NewGuid();
            _expectedCommand = new ChangeEmployeeIsActive(_employeeId, false);
        };

        Because of = () => { _employeesController.InactivateEmployee(_employeeId).Await(); };

        It should_dispatch_change_is_active_command_with_false = () =>
        {
            Mock.Get(_commandDispatcher).Verify(
                dispatcher =>
                    dispatcher.Dispatch(ItVerify.Is<ChangeEmployeeIsActive>(changeIsActiveCommand =>
                        _expectedCommand.ShouldBeEquivalent(changeIsActiveCommand))));
        };
    }
}