using System;
using Domain.Employees.Commands;
using FizzWare.NBuilder;
using Machine.Specifications;
using Moq;
using Testing.Utilities;
using Web.Employees;
using Web.Employees.Requests;
using It = Machine.Specifications.It;

namespace Web.Specs.Employees
{
    [Subject(typeof(EmployeesController))]
    public class when_changing_employee_address : given_an_employee_controller_context
    {
        static UpdateAddressRequest _updateAddressRequest;
        static Guid _id;

        Establish context = () =>
        {
            _id = new Guid("0f8fad5b-d9cb-469f-a165-70867728950e");

            _updateAddressRequest = Builder<UpdateAddressRequest>.CreateNew()
                .Build();
        };

        Because of = () => { _employeesController.ChangeAddress(_id, _updateAddressRequest).Await(); };

        It should_dispatch_update_employee_address_command = () =>
            Mock.Get(_commandDispatcher)
                .ShouldDispatchACommandLike<ChangeEmployeeAddress>(command =>
                    command.Id == _id &&
                    command.Address == _updateAddressRequest.Address);
    }
}