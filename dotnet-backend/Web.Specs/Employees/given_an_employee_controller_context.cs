using System;
using AutoMapper;
using Avenue.Commands;
using Avenue.Dispatch;
using Avenue.Domain;
using Data.Projections;
using Domain.Common.Services;
using Domain.Employees;
using Machine.Specifications;
using Moq;
using Web.Employees;

namespace Web.Specs.Employees
{
    public class given_an_employee_controller_context
    {
        protected static EmployeesController _employeesController;
        protected static IDispatcher<ICommand> _commandDispatcher;
        //EE16:START
        protected static IReadOnlyRepository<EmployeeListing> _readOnlyRepository;
        //EE16:END
        //RP16:START
        // protected static IReadOnlyRepository<Employee> _readOnlyRepository;
        //RP16:END
        protected static IIdentityGenerator<Guid> _identityGenerator;

        Establish _context = () =>
        {
            _commandDispatcher = Mock.Of<IDispatcher<ICommand>>();
            //EE17:START
            _readOnlyRepository = Mock.Of<IReadOnlyRepository<EmployeeListing>>();
            //EE17:END
            //RP17:START
            // _readOnlyRepository = Mock.Of<IReadOnlyRepository<Employee>>();
            //RP17:END
            _identityGenerator = Mock.Of<IIdentityGenerator<Guid>>();

            _employeesController =
                new EmployeesController(_commandDispatcher, _readOnlyRepository, _identityGenerator);
        };
    }
}