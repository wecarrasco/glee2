using System;
using Domain.Employees.Commands;
using FizzWare.NBuilder;
using Machine.Specifications;
using Moq;
using Testing.Utilities;
using Web.Employees;
using Web.Employees.Requests;
using It = Machine.Specifications.It;


namespace Web.Specs.Employees
{
    [Subject(typeof(EmployeesController))]
    public class when_changing_employee_names : given_an_employee_controller_context
    {
        static UpdateNamesRequest _updateNamesRequest;
        static Guid _id;

        Establish context = () =>
        {
            _id = new Guid("0f8fad5b-d9cb-469f-a165-70867728950e");

            _updateNamesRequest = Builder<UpdateNamesRequest>.CreateNew()
                .Build();
        };

        Because of = () => { _employeesController.ChangeNames(_id, _updateNamesRequest).Await(); };

        It should_dispatch_update_employee_names_command = () =>
            Mock.Get(_commandDispatcher)
                .ShouldDispatchACommandLike<ChangeEmployeeNames>(x =>
                    x.Id == _id &&
                    x.FirstName == _updateNamesRequest.FirstName &&
                    x.MiddleName == _updateNamesRequest.MiddleName &&
                    x.LastName == _updateNamesRequest.LastName &&
                    x.SecondLastName == _updateNamesRequest.SecondLastName);
    }
}