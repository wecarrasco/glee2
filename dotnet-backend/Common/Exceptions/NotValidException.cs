using System;
using System.Collections.Generic;
using Common.Commands.Validators;

namespace Common.Exceptions
{
    public class NotValidException : Exception
    {
        public NotValidException(IEnumerable<ValidationError> errors)
        {
            Errors = errors;
        }

        public IEnumerable<ValidationError>  Errors { get;  }
    }
}