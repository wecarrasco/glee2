using System.Threading.Tasks;

namespace Common.Commands.Validators
{
    public interface IValidationComponent
    {
        
    }
    public interface IValidationComponent<in TCommand> : IValidationComponent
    {
        Task<IValidationResult> Validate(TCommand command);
    }
}