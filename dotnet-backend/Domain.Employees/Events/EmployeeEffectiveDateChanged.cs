using System;
using Avenue.Domain;

namespace Domain.Employees.Events
{
    public class EmployeeEffectiveDateChanged : IDomainEvent
    {
        protected EmployeeEffectiveDateChanged()
        {
            
        }

        public Guid EmployeeId { get; protected set; } = Guid.Empty;
        public DateTime EffectiveDate { get; set; } = new DateTime();

        public EmployeeEffectiveDateChanged(Guid id, DateTime effectiveDate)
        {
            EmployeeId = id;
            EffectiveDate = effectiveDate;
        }
    }
}