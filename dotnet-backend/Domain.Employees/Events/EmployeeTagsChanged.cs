using System;
using Avenue.Domain;

namespace Domain.Employees.Events
{
    public class EmployeeTagsChanged : IDomainEvent
    {
        protected EmployeeTagsChanged()
        {
            
        }
        public Guid EmployeeId { get; protected set; } = Guid.Empty;
        public string Tags { get; } = "";


        public EmployeeTagsChanged(Guid employeeId, string tags)
        {
            EmployeeId = employeeId;
            Tags = tags;
        }
    }
}