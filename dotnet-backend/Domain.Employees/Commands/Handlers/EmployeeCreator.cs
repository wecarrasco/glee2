using System;
using System.Threading.Tasks;
using Avenue.Commands;
using Avenue.Domain;

namespace Domain.Employees.Commands.Handlers
{
    public class EmployeeCreator : ICommandHandler<CreateEmployee>
    {
        readonly IWritableRepository<Employee> _writableRepository;

        public EmployeeCreator(IWritableRepository<Employee> writableRepository)
        {
            _writableRepository = writableRepository;
        }

        public async Task Handle(CreateEmployee command)
        {
            if(command==null)
                throw new ArgumentNullException(nameof(command),$"{nameof(command)} cannot be null");
            var employee = new Employee(command.Id, command.FirstName, command.MiddleName, command.LastName,
                command.SecondLastName,
                command.DisplayName, command.CompanyEmail, command.PersonalEmail, command.Birthdate, command.StartDate,
                command.Address, command.PhoneNumber, command.BankName, command.AccountNumber, command.Gender,
                command.Tags, command.Country, command.Region, command.City, command.Salary, command.EffectiveDate,
                command.SalaryType);

            await _writableRepository.Create(employee);
        }
    }
}