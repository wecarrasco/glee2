using System;
using System.Threading.Tasks;
using Avenue.Commands;
using Avenue.Domain;

namespace Domain.Employees.Commands.Handlers
{
    public class EmployeeIsActiveChange : ICommandHandler<ChangeEmployeeIsActive>
    {
        readonly IWritableRepository<Employee> _repo;

        public EmployeeIsActiveChange(IWritableRepository<Employee> repo)
        {
            _repo = repo;
        }
        
        public async Task Handle(ChangeEmployeeIsActive command)
        {
            if(command==null)
                throw new ArgumentNullException(nameof(command),$"{nameof(command)} cannot be null");
            var employee = await _repo.Find(command.Id);
            employee.ChangeIsActive(command.IsActive);
            await _repo.Update(employee);
        }
    }
}