using System;
using Avenue.Commands;

namespace Domain.Employees.Commands
{
    public class ChangeEmployeeCompanyEmail : ICommand
    {
        public Guid Id { get; }
        public string CompanyEmail { get; }

        public ChangeEmployeeCompanyEmail(Guid id, string companyEmail)
        {
            Id = id;
            CompanyEmail = companyEmail;
        }
    }
}