using Domain.Common;
using FluentValidation;

namespace Domain.Employees.Commands.Validators.ForCreateEmployee
{
    public class ValidateCreateEmployee : CommandValidatorBase<CreateEmployee>
    {
        public ValidateCreateEmployee()
        {
            RuleFor(employee => employee.FirstName).NotEmpty();
            RuleFor(e => e.LastName).NotEmpty();
            RuleFor(e => e.CompanyEmail).NotEmpty();
            RuleFor(e => e.Gender).NotEmpty();
            RuleFor(e => e.StartDate).NotEmpty();
            RuleFor(e => e.SalaryType).NotEmpty();
            RuleFor(e => e.Salary).NotEmpty();
            RuleFor(e => e.EffectiveDate).NotEmpty();
            RuleFor(e => e.Country).NotEmpty();
            RuleFor(e => e.Region).NotEmpty();
            RuleFor(e => e.City).NotEmpty();
            RuleFor(e => e.DisplayName).NotEmpty();
        }
    }
}