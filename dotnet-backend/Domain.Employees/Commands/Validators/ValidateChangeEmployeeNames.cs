using Domain.Common;
using FluentValidation;

namespace Domain.Employees.Commands.Validators
{
    public class ValidateChangeEmployeeNames : CommandValidatorBase<ChangeEmployeeNames>
    {
        public ValidateChangeEmployeeNames()
        {
            RuleFor(x => x.FirstName).NotEmpty();
            RuleFor(x => x.LastName).NotEmpty();
        }
    }
}