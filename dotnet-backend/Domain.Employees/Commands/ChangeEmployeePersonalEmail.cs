using System;
using Avenue.Commands;

namespace Domain.Employees.Commands
{
    public class ChangeEmployeePersonalEmail : ICommand
    {
        public Guid Id { get; }
        public string PersonalEmail { get; }

        public ChangeEmployeePersonalEmail(Guid id, string personalEmail)
        {
            Id = id;
            PersonalEmail = personalEmail;
        }
    }
}