using System;
using Avenue.Commands;

namespace Domain.Employees.Commands
{
    public class RemoveEmployee : ICommand
    {
        protected RemoveEmployee()
        {
        }

        public Guid Id { get; private set; } = Guid.Empty;

        public RemoveEmployee(Guid id)
        {
            Id = id;
        }
    }
}