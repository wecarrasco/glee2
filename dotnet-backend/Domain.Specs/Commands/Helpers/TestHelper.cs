using FluentAssertions;

namespace Domain.Specs.Commands.Helpers
{
    public static class TestHelper
    {
        public static bool ShouldBeEquivalent(this object original, object next)
        {
            original.Should().BeEquivalentTo(next);
            return true;
        }
        
    }
}