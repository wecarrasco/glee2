using System;
using System.Linq;
using Domain.Employees.Events;
using FizzWare.NBuilder;
using FluentAssertions;
using Machine.Specifications;

namespace Domain.Employees.Specs
{
    public class when_changing_the_BirthDate
    {
        static Employee _systemUnderTest;
        static DateTime _newBirthDate;

        Establish _context = () =>
        {
            _newBirthDate = DateTime.Now;
            _systemUnderTest = Builder<Employee>.CreateNew()
                .With(x => x.Id, Guid.NewGuid())
                .Build();
        };

        Because of = () => { _systemUnderTest.ChangeBirthDate(_newBirthDate); };

        It should_change_the_birthdate = () => { _systemUnderTest.Birthdate.Should().Be(_newBirthDate); };

        It should_notify_the_world_of_the_change = () =>
            _systemUnderTest.GetChanges().FirstOrDefault().Should()
                .BeEquivalentTo(new EmployeeBirthDateChanged(_systemUnderTest.Id, _newBirthDate));
    }
}