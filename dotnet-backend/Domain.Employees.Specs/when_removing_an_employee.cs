using System;
using Domain.Employees.Events;
using FizzWare.NBuilder;
using FluentAssertions;
using Machine.Specifications;

namespace Domain.Employees.Specs
{
    public class when_removing_an_employee
    {
        Establish _context = () =>
        {
            _id = Guid.NewGuid();
            _systemUnderTest = Builder<Employee>.CreateNew()
                .With(x => x.Id, _id)
                .With(x => x.Removed, false)
                .Build();
        };

        Because of = () => { _systemUnderTest.Remove(); };

        It should_mark_the_employee_as_removed = () => _systemUnderTest.Removed.Should().BeTrue();

        It should_raise_an_event = () =>
            _systemUnderTest.GetChanges().Should().ContainEquivalentOf(new EmployeeRemoved(_id));

        static Employee _systemUnderTest;
        static Guid _id;
    }
}