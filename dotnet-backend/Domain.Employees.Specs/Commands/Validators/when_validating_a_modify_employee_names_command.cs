using System;
using Domain.Employees.Commands;
using Domain.Employees.Commands.Validators;
using FizzWare.NBuilder;
using Machine.Specifications;
using Testing.Utilities;

namespace Domain.Employees.Specs.Commands.Validators
{
    public class when_validating_a_modify_employee_names_command
    {
        static ValidateChangeEmployeeNames _validator;
        static ChangeEmployeeNames _command;

        Establish context = () =>
        {
            _validator = new ValidateChangeEmployeeNames();
            _command = Builder<ChangeEmployeeNames>.CreateNew()
                .With(employee => employee.FirstName, "")
                .With(e => e.LastName, "")
                .Build();
        };

        Because of = () =>
            _result = Catch.Exception(() => _validator.Validate(_command).Await());

        It should_return_errors_for_first_name = () =>
            _result.ShouldIncludeValidationFailureLike(x => x.PropertyName == "FirstName");

        It should_return_errors_for_last_name = () =>
            _result.ShouldIncludeValidationFailureLike(x => x.PropertyName == "LastName");

        static Exception _result;
    }
}