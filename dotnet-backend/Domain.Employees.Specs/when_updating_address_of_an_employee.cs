using System;
using System.Linq;
using Domain.Employees.Events;
using FizzWare.NBuilder;
using FluentAssertions;
using Machine.Specifications;

namespace Domain.Employees.Specs
{
    public class when_updating_address_of_an_employee
    {
        static Employee _employee;

        static string _address;
        static string _city;
        static string _region;
        static string _country;

        Establish context = () =>
        {
            _address = "Barrio La Tigra";
            _city = "Tegucigalpa";
            _region = "Centro";
            _country = "Honduras";

            _employee = Builder<Employee>.CreateNew()
                .With(x => x.Id, Guid.NewGuid())
                .Build();
        };

        Because of = () => { _employee.ChangeAddress(_address, _city, _region, _country); };

        It should_publish_update_event = () =>
        {
            _employee.GetChanges().FirstOrDefault().Should()
                .BeEquivalentTo(new EmployeeAddressChanged(_employee.Id, _address, _city, _region, _country));
        };
    }
}