using Avenue.Testing.Moq;
using Data.Projections.EventHandlers;
using Domain.Employees.Events;
using FizzWare.NBuilder;
using Machine.Specifications;
using Moq;
using It = Machine.Specifications.It;

namespace Data.Projections.Specs
{
    public class when_new_employeelistingeventhandlers : given_an_employee_listing_event_handler_context
    {
        Establish _context = () =>
        {
            _employeeCreated = Builder<EmployeeCreated>.CreateNew().Build();

            _expectedEmployeeListing = new EmployeeListing(_employeeCreated.EmployeeId, _employeeCreated.FirstName,
                _employeeCreated.MiddleName, _employeeCreated.LastName, _employeeCreated.SecondLastName,
                _employeeCreated.DisplayName, _employeeCreated.CompanyEmail, _employeeCreated.PersonalEmail,
                _employeeCreated.Birthdate, _employeeCreated.StartDate, _employeeCreated.Address,
                _employeeCreated.PhoneNumber, _employeeCreated.BankName, _employeeCreated.AccountNumber,
                _employeeCreated.Gender, _employeeCreated.Tags, _employeeCreated.Country, _employeeCreated.Region,
                _employeeCreated.City, _employeeCreated.Salary, _employeeCreated.SalaryType,
                _employeeCreated.EffectiveDate, true);
            
            _systemUnderTest = new EmployeeListingEventHandlers(_writableRepository);
        };

        Because of = () => { _systemUnderTest.Handle(_employeeCreated).Wait();};

        It should_be_able_to_create_new_employees = () => { 
            Mock.Get(_writableRepository).Verify(x => x.Create(
                WithEventHandlersFromSome<EmployeeListing>.Like(_expectedEmployeeListing)));
        };
        static EmployeeCreated _employeeCreated;
        static EmployeeListing _expectedEmployeeListing;
    }
}