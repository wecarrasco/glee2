using System;
using Domain.Employees.Events;
using FluentAssertions;
using Machine.Specifications;

namespace Data.Projections.Specs
{
    public class when_handling_employee_names_changed : given_update_handler_context
    {
        Establish _context = () =>
        {
            _employeeNamesChanged = new EmployeeNamesChanged(
                Guid.NewGuid(),
                "Test",
                "Test",
                "Testing",
                "Testing");
        };

        Because of = () => { _systemUnderTest.Handle(_employeeNamesChanged); };

        It should_call_update_repository_with_new_employee_structure = () =>
        {
            _writableRepository.Verify(repo=>repo.Update(Moq.It.IsAny<EmployeeListing>()));
            _argumentListing.FirstName.Should().Be("Test");
            _argumentListing.MiddleName.Should().Be("Test");
            _argumentListing.LastName.Should().Be("Testing");
            _argumentListing.SecondLastName.Should().Be("Testing");
        };
        static EmployeeNamesChanged _employeeNamesChanged;
    }
}