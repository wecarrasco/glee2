using System;
using Avenue.Testing.Moq;
using Domain.Employees.Events;
using FizzWare.NBuilder;
using Machine.Specifications;
using Moq;
using It = Machine.Specifications.It;

namespace Data.Projections.Specs
{
    public class when_responding_to_a_display_name_change : given_an_employee_listing_event_handler_context
    {
        Establish _context = () =>
        {
            _employeeId = Guid.NewGuid();
            _event = new EmployeeDisplayNameChanged(_employeeId, "new display name");

            var employeeListing = Builder<EmployeeListing>.CreateNew().Build();
            Mock.Get(_writableRepository).Setup(x => x.Find(_employeeId)).ReturnsAsync(employeeListing);
        };

        Because of = () => { _systemUnderTest.Handle(_event).Wait(); };

        It should_update_the_display_name_in_the_repo = () =>
        {
            Mock.Get(_writableRepository).Verify(x =>
                x.Update(WithEventHandlersFromSome<EmployeeListing>.With(e => e.DisplayName == _event.DisplayName)));
        };

        static EmployeeDisplayNameChanged _event;
        static Guid _employeeId;
    }
}