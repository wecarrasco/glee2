using System;
using Domain.Employees.Events;
using FluentAssertions;
using Machine.Specifications;

namespace Data.Projections.Specs
{
    public class when_handling_employee_displayname_changed : given_update_handler_context
    {
        Establish _context = () =>
        {
            _employeeDisplayNameChanged = new EmployeeDisplayNameChanged(
                Guid.NewGuid(),"Test Test Testing Testing");
        };

        Because of = () => { _systemUnderTest.Handle(_employeeDisplayNameChanged); };

        It should_call_update_repository_with_new_employee_structure = () =>
        {
            _writableRepository.Verify(repo=>repo.Update(Moq.It.IsAny<EmployeeListing>()));
            _argumentListing.DisplayName.Should().Be("Test Test Testing Testing");
        };
        static EmployeeDisplayNameChanged _employeeDisplayNameChanged;
    }
}