# Glee Dotnet Core Backend

## CI/CD
...

## Review Apps
..

## Environment Variables

BUILD_VERSION - This env variable should be set during CI/CD in the build steps so that the version makes it into the `Web/Web.csproj` file and eventually is made available in the `/api/status` endpoint.

## Setup
Create your own  a copy of the environment variable files. You can do so by running `$ cp Web/develop.env Web/.env` while inside the `dotnet-backend` folder. 


You'll need to add env variables for the authentication scheme of your choise (or both).

Current authentication schemes supported: 
- HS256   (env: JWT_HS_SECRET_KEY - string)
- RS256   (env: JWT_RS_PUBLIC_KEY -  [xml](https://superdry.apphb.com/tools/online-rsa-key-converter))

You'll need to add the corresponding env variables to your `.env` file inside the `/Web` folder.

## Docker
To run the project on a docker local environment in watch mode, run `docker-compose up` inside this folder. 
To run the project on a "deployment" environment, run `docker-compose -f docker-compose-deployment.yaml up --build
` to use the alternative deployment dockerfile.

**Docker support is experimental at this moment. We may retake this once Rider is able to debug inside docker-compose.**

## Building from the Command Line

# Run Cake
The sh script below will install any needed dependencies and will attempt to build the project.

- for linux/MacOs:
```
cd dotnet-backend
./build.sh
```
- on Windows:
```
dotnet build 
```

## Development Workflow Comparison 
### Event Sourcing vs Relational

TLDR - Event sourcing adds two steps to your workflow, but huge benefits. For more, see [this article](https://microservices.io/patterns/data/event-sourcing.html).

Add a feature - Promote Employee - With Event Sourcing
(recommended)
1. Add cucumber test for the change and leave failing
2. Unit test endpoint on controller
3. Endpoint on controller
4. Create command
5. Unit test command handler
6. Create command handler
7. Unit test new method on employee entity
8. Add method to employee entity (changes property)
9. Add domain event
10. Unit test domain event handler
11. Add domain event handler to modify employee listing projection
12. Add migration for new column on employee listing
13. Make sure cucumber test passes

Add a feature - Promote Employee - With Relational
1. Add cucumber test for the change and leave failing
2. Unit test endpoint on controller
3. Endpoint on controller
4. Create command
5. Unit test command handler
6. Create command handler
7. Unit test new method on employee entity
8. Add migration for new column on employee listing
9. Add method to employee entity (changes property)
10. Add domain event
11. Make sure cucumber test passes

## API docs
 The API documentation can be accessed using Swagger via <url>/swagger.
 You can add a JWT token to your requests by [generating one](http://jwtbuilder.jamiekurtz.com) and adding it to the `Authorize` section.

## Event Sourcing or Traditional Persistence
At the start of a project you can choose between having event sourcing or traditional persitence for the data persistence layer.
The boilerplate code contains various segments that have been commented out with the tags
**EE** for event sourcing and **RP** for Traditional Persistence, followed by a number, you have 
to uncomment one of the two tags depending on the type of persistence you want to
use and delete the other commented code. For the boiler plate you will find this tags
in this files:
1. Web/Infraestructure/Bootstrapping/CommandEventSystemBootstrapper.cs
2. Data.Projections/Migrations/00000000001_AddEmployeeListing.cs
3. Data.Projections/Validations/VerifyNotDuplicate.cs
4. Data.Projections.Specs/when_verifying_with_duplicate_employee_name.cs
5. Web/Employees/EmployeesController.cs
6. Web.Specs/Employees/given_an_employee_controller_context.cs
7. Web.Specs/Employees/when_getting_all_employees.cs
8. Web.Specs/Employees/when_getting_all_employees_by_parameter.cs
9. Web.Specs/Employees/when_getting_one_employee_by_id.cs

The database created for event sourcing is not compatible with traditional persistence
therefore it is not recommended to change the type of data persistence in the 
middle of a project. 

Rename the Data.Projections project to Data.Employee and delete the IProjection Interface 
  