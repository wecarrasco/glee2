require('dotenv').config()

module.exports = {
  mode: 'spa',
  /*
   ** Headers of the page
   */
  head: {
    title: 'Glee2' || process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content:
          'The AA Nuxt Boilerplate' || process.env.npm_package_description || ''
      }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },
  env: {
    BACKEND_URL: process.env.BACKEND_URL,
    AUTH0_DOMAIN: process.env.AUTH0_DOMAIN,
    AUTH0_CLIENT_ID: process.env.AUTH0_CLIENT_ID,
    AUTH0_AUDIENCE: process.env.AUTH0_AUDIENCE
  },
  /*
   ** Customize the progress-bar color
   ** Plugins to load before mounting the App
   */
  loading: {
    duration: 10000, // ms
    height: '10px',
    color: '#fff'
  },
  /*
   ** Global CSS
   */
  css: ['@/assets/scss/index.scss', '@/assets/css/index.css'],
  plugins: [
    '@/plugins/Vuelidate',
    '@/plugins/Notifications',
    '@/plugins/Moment',
    '@/plugins/Auth0',
    '@/plugins/HttpService'
  ],
  router: {
    middleware: ['isAuthenticated'],
    extendRoutes(routes) {
      routes.push({ path: '/', redirect: '/employees' })
    }
  },
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: ['@nuxt/typescript-build'],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://bootstrap-vue.js.org
    'bootstrap-vue/nuxt',
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    // Doc: https://auth.nuxtjs.org/#getting-started
    // '@nuxtjs/auth',
    // Doc: https://github.com/nuxt-community/dotenv-module
    '@nuxtjs/dotenv'
  ],
  bootstrapVue: {
    bootstrapCSS: false, // Or `css: false`
    bootstrapVueCSS: false // Or `bvCSS: false`
  },
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {},
  /*
   ** Auth module configuration
   ** See https://auth.nuxtjs.org/guide/setup.html
   */
  // auth: {
  //   plugins: ['@/plugins/Logout', '@/plugins/HttpService'],
  //   redirect: {
  //     login: '/', // redirect user when not connected
  //     callback: '/successful-login'
  //   },
  //   strategies: {
  //     local: false,
  //     auth0: {
  //       domain: process.env.AUTH0_DOMAIN,
  //       client_id: process.env.AUTH0_CLIENT_ID,
  //       audience: process.env.AUTH0_AUDIENCE
  //     }
  //   }
  // },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue|ts)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
