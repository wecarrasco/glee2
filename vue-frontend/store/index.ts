import Vuex, { Store } from 'vuex'
import EmployeeModule from './modules/employee'
import TestModule from './tryingStoreTest'
import Auth0Module from './modules/auth0'

interface RootState {}
export const store = new Vuex.Store<RootState>({
  actions: {
    nuxtServerInit: () => {}
  },
  modules: {
    employee: EmployeeModule,
    testing: TestModule,
    auth0: Auth0Module
  }
})
const createStore = (): Store<RootState> => {
  return store
}
export default createStore
