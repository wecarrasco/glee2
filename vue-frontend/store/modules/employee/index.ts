import { VuexModule, Module, Mutation, Action } from 'vuex-module-decorators'
import { pickAll } from 'ramda'
import {
  GET_EMPLOYEES_SUCCESS,
  GET_EMPLOYEE_SUCCESS,
  EDIT_EMPLOYEE_SUCCESS,
  GET_EMPLOYEES,
  SET_EMPLOYEES_PAGINATION_DATA
} from '@/store/modules/employee/mutation-types'
import {
  Employee,
  CreateEmployee,
  EditEmployeePayload,
  GetResponse,
  QueryEmployee,
  Pagination
} from '@/types/employee'
import { havePropertiesChanged } from '@/utils'
import { httpService } from '@/services/HttpService/instance'

interface EmployeeState {
  employees: Employee[]
  employee?: Employee
  paginationData: Pagination
}

@Module({
  name: 'employee',
  namespaced: true,
  stateFactory: true
})
// eslint-disable-next-line @typescript-eslint/no-unused-vars, no-unused-vars
export default class EmployeeModule extends VuexModule
  implements EmployeeState {
  public employees = [] as Employee[]
  public employee?: Employee
  public paginationData: Pagination = {
    currentPage: 1,
    lastPage: 1,
    perPage: 5,
    total: 0
  };

  @Mutation
  [GET_EMPLOYEES]() {
    this.employees = [] as Employee[]
    this.employee = undefined
  }

  @Mutation
  [GET_EMPLOYEES_SUCCESS](data: Employee[]) {
    this.employees = data
    this.employee = undefined
  }

  get activeEmployee() {
    return this.employee
  }

  @Mutation
  [GET_EMPLOYEE_SUCCESS](data: Employee) {
    this.employee = data
  }

  @Mutation
  [SET_EMPLOYEES_PAGINATION_DATA]({
    currentPage,
    lastPage,
    perPage,
    total
  }: Pagination) {
    this.paginationData = { currentPage, lastPage, perPage, total }
  }

  @Mutation
  [EDIT_EMPLOYEE_SUCCESS]() {
    this.employee = undefined
  }

  @Action
  getEmployees({
    currentPage,
    filterText,
    pageSize
  }: QueryEmployee = {}): Promise<void> {
    this.context.commit(GET_EMPLOYEES)
    return httpService
      .get<GetResponse<Employee>>('employees', {
        params: {
          pageNumber: currentPage || this.paginationData.currentPage,
          nameSearch: filterText,
          pageSize: pageSize || this.paginationData.perPage
        }
      })
      .then((res: GetResponse<Employee>) => {
        this.context.commit(GET_EMPLOYEES_SUCCESS, res.data)
        this.context.commit(SET_EMPLOYEES_PAGINATION_DATA, res)
      })
  }

  @Action
  getEmployee(id: string): Promise<Employee> {
    return httpService
      .get<Employee>(`employees/${id}`)
      .then((res: Employee) => {
        this.context.commit(GET_EMPLOYEE_SUCCESS, res)
        return res
      })
  }

  // RawError required to display error message on notification
  @Action({ rawError: true })
  createEmployee(data: CreateEmployee): Promise<void> {
    return httpService.post('employees', data).then(() => {
      // @ts-ignore
      // eslint-disable-next-line no-undef
      $nuxt.$router.push('/employees')
    })
  }

  @Action
  editEmployeeField({
    payload,
    fields,
    endpoint
  }: {
    payload: EditEmployeePayload
    fields: string[]
    endpoint: string
  }): Promise<boolean> {
    if (havePropertiesChanged(fields, this.activeEmployee!, payload.data)) {
      return httpService
        .put(
          `employees/${payload.id}/${endpoint}`,
          pickAll(fields, payload.data)
        )
        .then(() => true)
    }
    return Promise.resolve(false)
  }

  @Action
  deactivateEmployee(id: string): Promise<void> {
    return httpService.put(`employees/${id}/inactive`, {}).then(() => {
      this.context.dispatch('getEmployees')
    })
  }

  @Action
  activateEmployee(id: string): Promise<void> {
    return httpService.put(`employees/${id}/active`, {}).then(() => {
      this.context.dispatch('getEmployees')
    })
  }

  @Action
  addTags(id: string, tags: string[]): Promise<void> {
    return httpService.put(`employees/${id}/tags`, tags).then(() => {
      this.context.dispatch('getEmployees')
    })
  }
}
