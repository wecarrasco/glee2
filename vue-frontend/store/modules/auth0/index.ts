import { VuexModule, Module, Mutation } from 'vuex-module-decorators'

interface State {
  loading: boolean
  isAuthenticated: boolean
  user: Object
  popupOpen: boolean
  permissions: string[]
}

@Module({
  name: 'auth0',
  namespaced: true,
  stateFactory: true
})
export default class Auth0Module extends VuexModule implements State {
  public loading = true
  public isAuthenticated = false
  public user = {}
  public popupOpen = false
  public permissions: string[] = []

  @Mutation
  setPopupOpen(payload: boolean) {
    this.popupOpen = !!payload
  }

  @Mutation
  setUser(payload: Object) {
    this.user = payload
  }

  @Mutation
  setIsAuthenticated(payload: boolean) {
    this.isAuthenticated = !!payload
  }

  @Mutation
  setLoading(payload: boolean) {
    this.loading = !!payload
  }

  @Mutation
  setPermissions(payload: string[]) {
    this.permissions = payload
  }
}
