import { NuxtAxiosInstance } from '@nuxtjs/axios'
import querystring from 'query-string'
import { AxiosInstance, AxiosResponse } from 'axios'
import { Auth0Instance } from '~/plugins/Auth0'

export namespace HttpServiceTypes {
  export interface Options {
    params: any
  }

  export interface Pagination {
    page?: number
    pageSize?: number
  }
}

export class HttpService {
  private axios: AxiosInstance
  constructor($axios: NuxtAxiosInstance, private $auth: Auth0Instance) {
    this.axios = $axios.create({
      baseURL: this.trimSlashes(this.backendUrl) + '/api'
    })
    this.axios.interceptors.response.use(
      this.onFulfilledRequest,
      this.onRejectedRequest
    )
  }

  public async get<T>(
    url: string,
    options?: HttpServiceTypes.Options
  ): Promise<T> {
    const token = await this.buildToken()
    let queryParams = ''
    if (options && options.params) {
      queryParams = this.getQueryStringFromParams(options.params)
    }

    const response = await this.axios.get<T>(
      `${this.trimSlashes(url)}${queryParams}`,
      {
        headers: {
          Authorization: token
        }
      }
    )
    return response.data
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  public async post(url: string, data: any) {
    if (this.hasToLogin()) {
      return this.$auth.loginWithRedirect()
    }
    const token = await this.buildToken(true)
    const response = await this.axios.post(this.trimSlashes(url), data, {
      headers: {
        Authorization: token,
        'Content-Type': 'application/json; charset=utf-8'
      }
    })

    return response.data
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  public async put(url: string, data: any) {
    if (this.hasToLogin()) {
      return this.$auth.loginWithRedirect()
    }
    const token = await this.buildToken(true)
    const response = await this.axios.put(this.trimSlashes(url), data, {
      headers: {
        Authorization: token,
        'Content-Type': 'application/json; charset=utf-8'
      }
    })
    return response.data
  }

  public async delete(url: string) {
    if (this.hasToLogin()) {
      return this.$auth.loginWithRedirect()
    }
    const token = await this.buildToken(true)

    const response = await this.axios.delete(this.trimSlashes(url), {
      headers: {
        Authorization: token
      }
    })

    return response.data
  }

  private trimSlashes(str: string): string {
    return str.replace(/\/+\s*$/, '').replace(/^\/+\s*/, '')
  }

  private onFulfilledRequest = (response: AxiosResponse) => response

  private onRejectedRequest(error: any) {
    if (!error.response) throw error
    switch (error.response.status) {
      case 401:
        return this.$auth.loginWithRedirect()
      case 500:
      case 404:
      case 400:
        throw error
    }
  }

  private hasToLogin(): boolean {
    return !this.$auth.isAuthenticated
  }

  private async buildToken(needsToken = false): Promise<string> {
    let token = ''
    try {
      token = await this.$auth.getTokenSilently()
    } catch (error) {
      if (error.error === 'login_required' && needsToken) {
        throw error
      }
    }
    return `Bearer ${token}`
  }

  private getQueryStringFromParams(params: object): string {
    return `?${querystring.stringify(params)}`
  }

  private get backendUrl() {
    const envBackendUrl = process.env.BACKEND_URL!
    const backendUrl = window.localStorage.getItem('backendUrl')
    if (!backendUrl) {
      window.localStorage.setItem('backendUrl', envBackendUrl)
    }
    return backendUrl || envBackendUrl
  }
}
