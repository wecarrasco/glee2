const path = require('path')

module.exports = ({ config }, env) => {
  config.module.rules.push(
    {
      test: /\.(ts|tsx)$/,
      use: [
        {
          loader: 'babel-loader'
        },
        {
          loader: 'ts-loader',
          options: {
            transpileOnly: true,
            appendTsSuffixTo: [/\.vue$/]
          }
        }
      ]
    },
    {
      test: /\.css$/,
      use: ['postcss-loader'],
      exclude: [path.resolve(__dirname, '../assets/css/tailwind.css')]
    },
    {
      test: /\.s?css$/,
      use: [
        'vue-style-loader',
        { loader: 'css-loader', options: { importLoaders: 1 } },
        'postcss-loader',
        { loader: 'sass-loader' }
      ],
      include: [path.resolve(__dirname, '../assets/scss/index.scss')]
    }
  )
  config.resolve.extensions.push('.ts', '.tsx')
  config.resolve.alias = {
    '@': path.dirname(path.resolve(__dirname)),
    vue$: 'vue/dist/vue.esm.js'
  }
  return config
}
