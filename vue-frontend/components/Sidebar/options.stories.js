import { withKnobs } from '@storybook/addon-knobs'
import Options from './Options.vue'

export default { title: 'Options', decorator: [withKnobs], parameters: {} }

export const asAComponent = () => ({
  components: { Options },
  data() {
    return {
      options: [
        {
          name: 'Employees',
          className: 'accordion-items is-active',
          suboptions: [
            {
              name: 'View & Manage',
              link: '/employees'
            },
            {
              name: 'Add New',
              link: '/employees/create'
            }
          ]
        },
        {
          name: 'Logout',
          className: 'logout'
        }
      ]
    }
  },
  template: `
    <Options :options="options"/>
  `
})
