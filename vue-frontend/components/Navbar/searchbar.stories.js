import { withKnobs } from '@storybook/addon-knobs'
import Searchbar from './Searchbar.vue'

export default {
  title: 'Searchbar',
  decorator: [withKnobs],
  parameters: {}
}

export const asAComponent = () => ({
  components: { Searchbar },
  template: `
    <Searchbar />
  `
})
