import { withKnobs } from '@storybook/addon-knobs'
import EmployeeNavbar from './EmployeeNavbar.vue'

export default {
  title: 'EmployeeNavbar',
  decorator: [withKnobs],
  parameters: {}
}

export const asAComponent = () => ({
  components: { EmployeeNavbar },
  data() {
    return {
      breadcrumbMenu: [
        {
          text: 'Employees',
          style: this.thirdLevelBreadcrumb
        },
        {
          text: 'View & Manage',
          style: this.thirdLevelBreadcrumb
        }
      ],
      pageTitle: 'View & Manage Employees'
    }
  },
  template: `
    <EmployeeNavbar :page-title="pageTitle" :breadcrumb-menu="breadcrumbMenu"/>
  `
})
