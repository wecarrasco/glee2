import { withKnobs } from '@storybook/addon-knobs'
import FilterBar from './FilterBar.vue'

export default {
  title: 'FilterBar',
  decorator: [withKnobs],
  parameters: {}
}

export const asAComponent = () => ({
  components: { FilterBar },
  template: `
    <FilterBar />
  `
})
