import Vuex, { ModuleTree } from 'vuex'
import VueMeta from 'vue-meta'
import { createLocalVue, mount, VueClass } from '@vue/test-utils'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import Vuelidate from 'vuelidate'
import TestingModule from '@/store/tryingStoreTest'

export const mockAuth = {
  loggedIn: true,
  user: { nickname: 'Nickname' },
  loginWith: jest.fn()
}

export default (
  VueComponent: VueClass<Vue>,
  props?: any,
  storeModules?: ModuleTree<any>,
  mockRoute?: any,
  attrs?: any
) => {
  const localVue = createLocalVue()
  localVue.use(Vuex)
  localVue.use(BootstrapVue)
  localVue.use(IconsPlugin)
  localVue.use(Vuelidate)
  localVue.use(VueMeta, { keyName: 'head' })

  const store = new Vuex.Store({
    modules: storeModules || {
      testing: TestingModule
    }
  })
  return mount(VueComponent, {
    propsData: { ...props },
    attrs,
    store,
    localVue,
    mocks: {
      $auth: mockAuth,
      $route: mockRoute
    }
  })
}
