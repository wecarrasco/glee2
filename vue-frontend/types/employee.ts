export interface GetResponse<T> extends Pagination {
  data: T
}

export interface Pagination {
  currentPage: number
  lastPage: number
  perPage: number
  total: number
}

export interface QueryEmployee {
  filterText?: string
  currentPage?: number
  pageSize?: number
}

export enum Gender {
  MALE = 'male',
  FEMALE = 'female'
}

export enum SalaryType {
  HOURLY = 'hourly',
  YEARLY = 'yearly'
}

export interface CreateEmployee {
  gender: Gender
  firstName: string
  lastName: string
  displayName: string
  companyEmail: string
  middleName?: string
  secondLastName?: string
  personalEmail?: string
  birthdate?: string
  startDate?: string
  address?: string
  phoneNumber?: string
  bankName?: string
  accountNumber?: string
  tags?: string
  country?: string
  region?: string
  city?: string
  salary?: number
  effectiveDate?: string
  salaryType?: SalaryType
}

export interface Employee {
  gender: Gender
  id: string
  firstName: string
  middleName: string | null
  lastName: string
  secondLastName: string | null
  displayName: string
  companyEmail: string
  personalEmail: string
  birthdate: string
  startDate: string
  address: string
  phoneNumber: string
  bankName: string
  accountNumber: string
  tags: string
  country: string
  region: string
  city: string
  salary: number
  effectiveDate: string
  salaryType: SalaryType
  removed: boolean
  isActive: boolean
}

export interface EditEmployeePayload {
  id: string
  data: CreateEmployee
}
