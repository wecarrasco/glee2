# nuxt-vue-frontend

### Environment Variables

Currently, projects needs the following env variables:

- BACKEND_URL=
- AUTH0_DOMAIN=
- AUTH0_CLIENT_ID=
- AUTH0_AUDIENCE=

## Build Setup

````

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
````

## Storybook support

### Run storybook

Uses port [9009](http://localhost:9009/).

```
npm run storybook
```

### build storybook

```
npm run build-storybook
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
