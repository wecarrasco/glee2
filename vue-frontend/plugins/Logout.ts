import { Plugin } from '@nuxt/types'

declare module 'vue/types/vue' {
  interface Vue {
    $completeLogout(): void
  }
}

declare module '@nuxt/types' {
  interface NuxtAppOptions {
    $completeLogout(): void
  }
}

declare module 'vuex/types/index' {
  interface Store<S> {
    $completeLogout(): void
  }
}

const logoutPlugin: Plugin = ({ app: { $auth } }, inject) => {
  const currentUrl = encodeURIComponent(window.location.origin)
  inject('completeLogout', () =>
    $auth
      .logout()
      .then(() =>
        window.open(
          `https://${process.env.AUTH0_DOMAIN}/v2/logout?returnTo=${currentUrl}`,
          '_self'
        )
      )
  )
}

export default logoutPlugin
